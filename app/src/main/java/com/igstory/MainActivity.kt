package com.igstory

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.viewpager2.widget.ViewPager2
import com.lib.storyview.StoryPagerWrapper
import com.lib.storyview.data.StoryItemModel
import com.lib.storyview.data.StoryType
import com.lib.storyview.StoryView
import com.lib.storyview.data.StoriesItemModel


class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val storyPager = findViewById<ViewPager2>(R.id.story_pager)

        val pagerWrapper = StoryPagerWrapper(storyPager, items)
        lifecycle.addObserver(pagerWrapper)
    }
}