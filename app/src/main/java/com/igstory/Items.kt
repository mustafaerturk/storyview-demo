package com.igstory

import com.lib.storyview.data.StoriesItemModel
import com.lib.storyview.data.StoryItemModel
import com.lib.storyview.data.StoryType

private val list1 = listOf(
    StoryItemModel(
        id = "1",
        type = StoryType.IMAGE,
        url = "https://i.picsum.photos/id/533/800/900.jpg?hmac=7xHVnjK6714iVcichXiDmwdqzUwg9AuR7Vrb795RDOM"
    ),
    StoryItemModel(
        id = "6",
        type = StoryType.IMAGE,
        url = "https://i.picsum.photos/id/987/800/900.jpg?hmac=C1EAG5eHSpKVTiB6GuszYiCDRDIQ0-sKWHg9aqQ654s"
    ),
    StoryItemModel(
        "10",
        StoryType.VIDEO,
        "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerMeltdowns.mp4"
    )
)

private val list2 = listOf(
    StoryItemModel(
        "17",
        StoryType.VIDEO,
        "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerMeltdowns.mp4"
    ),
    StoryItemModel(
        id = "6",
        type = StoryType.IMAGE,
        url = "https://i.picsum.photos/id/326/800/900.jpg?hmac=i0WC7L9ItxllL91LYwvewcMqyLmrAMBJj0rP-dfM5ZM"
    ),
    StoryItemModel(
        "10",
        StoryType.VIDEO,
        "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerMeltdowns.mp4"
    )
)

private val list3 = listOf(
    StoryItemModel(
        id = "1",
        type = StoryType.IMAGE,
        url = "https://i.picsum.photos/id/349/800/900.jpg?hmac=VdJVSOkQOJzx65S7O4cDtXp34ExWpxySgsfdxgLCvnM"
    ),
    StoryItemModel(
        id = "8",
        type = StoryType.IMAGE,
        url = "https://i.picsum.photos/id/318/800/900.jpg?hmac=nMUKClmZgZMCTp4kRXgep5awjhWyQZdVTLyZjk3rhzU"
    ),
    StoryItemModel(
        id = "2",
        type = StoryType.IMAGE,
        url = "https://i.picsum.photos/id/554/800/900.jpg?hmac=nuPlmzQPIogduMAzbZOEORAQl0OaIQV4GzrZj26bQEI"
    ),
    StoryItemModel(
        id = "97",
        type = StoryType.IMAGE,
        url = "https://i.picsum.photos/id/954/800/900.jpg?hmac=GfNhK8o7UmJU0RNPhqLIBygSADrUxn_vGsf6lBTCCCQ"
    ),
    StoryItemModel(
        id = "45",
        type = StoryType.IMAGE,
        url = "https://i.picsum.photos/id/268/800/900.jpg?hmac=2EmRly4VhU1qTP20iVaayZ46ANUOeW6Nc7k5PBG-cWo"
    )
)

val items = listOf(
    StoriesItemModel(0, list1),
    StoriesItemModel(1, list2),
    StoriesItemModel(2, list3),
    StoriesItemModel(3, list2),
    StoriesItemModel(4, list3)
)