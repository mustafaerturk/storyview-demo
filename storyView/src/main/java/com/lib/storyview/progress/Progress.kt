package com.lib.storyview.progress

interface Progress {

    fun stop()

    fun resume()

    fun start(duration: Long)

    fun completed()

    fun reset()

    fun restart()
}

enum class ProgressState {
    NOT_STARTED,
    PLAYING,
    STOPPED,
    FINISHED
}