package com.lib.storyview.progress

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.widget.ProgressBar
import com.lib.storyview.StoryPageController
import com.lib.storyview.utils.StoryAction
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.functions.Consumer
import java.util.concurrent.TimeUnit

@SuppressLint("ViewConstructor")
class StoryProgressView @JvmOverloads constructor(
    context: Context,
    private val page: Int,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = android.R.attr.progressBarStyleHorizontal
) : ProgressBar(context, attrs, defStyleAttr), Progress {

    private var disposable: Disposable? = null
    private var passedTime: Long = 0

    private val consumer = Consumer<Long> {
        progress = it.toInt()
    }

    var status: ProgressState = ProgressState.NOT_STARTED

    init {
        initView()
    }

    private fun initView() {
        progress = 0
    }

    override fun stop() {
        status = ProgressState.STOPPED
        disposable?.dispose()
    }

    override fun resume() {
        val remainingTime = max - passedTime
        startAnimation(remainingTime)
    }

    override fun start(duration: Long) {
        this.max = duration.toInt() * 1000
        startAnimation( this.max.toLong())
    }

    override fun completed() {
        status = ProgressState.FINISHED
        progress = max
        passedTime = progress.toLong()
        disposable?.dispose()
    }

    override fun reset() {
        status = ProgressState.NOT_STARTED
        progress = 0
        passedTime = 0
        disposable?.dispose()
    }

    override fun restart() {
        reset()
        startAnimation(max.toLong())
    }

    private fun startAnimation(playDuration: Long) {
        disposable = Observable.interval(1, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .take(playDuration)
            .map {
                passedTime += 1
                passedTime
            }
            .doOnSubscribe {
                status = ProgressState.PLAYING
            }
            .doOnComplete {
                completed()
                StoryPageController.submit(StoryAction.ToNextItemAction(page))
            }
            .subscribe(consumer)

        StoryPageController.add(disposable!!)
    }
}