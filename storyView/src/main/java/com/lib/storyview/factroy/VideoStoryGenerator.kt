package com.lib.storyview.factroy

import android.content.Context
import android.net.Uri
import android.view.View
import com.devbrackets.android.exomedia.ui.widget.VideoView
import com.lib.storyview.StoryPageController
import com.lib.storyview.utils.StoryAction


class VideoStoryGenerator(
    override val context: Context,
) : StoryGenerator, StoryStoppable {

    private val videoView = VideoView(context)
    private var currentPosition: Long = 0
    private var currentUrl = ""
    private var currentPage = 0

    override fun create(url: String, page: Int): View {
        load(url, page)
        return videoView
    }

    private fun load(url: String, page: Int) {

        currentUrl = url
        currentPage = page

        videoView.setControls(null)
        videoView.setOnPreparedListener {
            StoryPageController.submit(StoryAction.StartProgress(page, videoView.duration / 1000))
            start()
        }

        videoView.setVideoURI(Uri.parse(url))
    }

    override fun release() {
        stop()
        videoView.release()
    }

    override fun stop() {
        currentPosition = videoView.currentPosition
        videoView.stopPlayback()
    }

    override fun start() {
        videoView.start()
    }

    override fun resume() {
        videoView.reset()
        load(currentUrl,currentPage)
        videoView.seekTo(currentPosition)
    }

    override fun restart() {
        videoView.reset()
        load(currentUrl,currentPage)
    }

}