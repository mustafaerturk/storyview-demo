package com.lib.storyview.factroy

import android.content.Context
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.lib.storyview.StoryPageController
import com.lib.storyview.utils.StoryAction

class ImageStoryGenerator(
    override val context: Context,
) : StoryGenerator {

    private val duration = 5L
    private val image = ImageView(context)

    override fun create(url: String, page: Int): View {
        Glide.with(context)
            .load(url)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true)
            .into(image)

        StoryPageController.submit(
            StoryAction.StartProgress(page, duration)
        )

        return image
    }
}