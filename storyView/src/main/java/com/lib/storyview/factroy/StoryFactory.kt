package com.lib.storyview.factroy

import android.content.Context
import android.view.View
import com.lib.storyview.data.StoryType

class StoryFactory(private val context: Context) {

    private val imageGenerator: ImageStoryGenerator by lazy {
        ImageStoryGenerator(context)
    }

    private val videoGenerator: VideoStoryGenerator by lazy {
        VideoStoryGenerator(context)
    }

    fun create(
        type: StoryType
    ): StoryGenerator {
        return when (type) {
            StoryType.IMAGE -> {
                imageGenerator
            }
            StoryType.VIDEO -> {
                videoGenerator
            }
        }
    }
}