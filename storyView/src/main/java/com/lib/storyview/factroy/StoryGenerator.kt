package com.lib.storyview.factroy

import android.content.Context
import android.view.View

interface StoryGenerator {

    val context: Context

    fun create(url: String, page: Int): View

}

interface StoryStoppable {

    fun stop()

    fun start()

    fun resume()

    fun restart()

    fun release()
}