package com.lib.storyview

import com.lib.storyview.utils.StoryAction
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.subjects.BehaviorSubject

object StoryPageController {

    private val subject =
        BehaviorSubject.create<StoryAction>()

    private val compositeDisposable = CompositeDisposable()

    init {
        subject.observeOn(AndroidSchedulers.mainThread())
        subject.doOnError {
            it.printStackTrace()
        }
    }

    fun submit(action: StoryAction) {
        subject.onNext(action)
    }

    fun observe(): Observable<StoryAction> {
        return subject
    }

    fun add(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    fun clear() {
        compositeDisposable.clear()
    }

    fun dispose() {
        compositeDisposable.dispose()
    }
}
