package com.lib.storyview.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.lib.storyview.R
import com.lib.storyview.StoryView
import com.lib.storyview.data.StoriesItemModel

class StoryHolder(
    itemView: View
) : RecyclerView.ViewHolder(itemView) {

    private val storyView = itemView.findViewById<StoryView>(R.id.story_view)
    private lateinit var storyItems: StoriesItemModel

    fun bind(items: StoriesItemModel, position: Int) {
        storyItems = items
        storyView.storiesItem = storyItems
        if (position == 0) {
            storyView.start()
        }
    }
}