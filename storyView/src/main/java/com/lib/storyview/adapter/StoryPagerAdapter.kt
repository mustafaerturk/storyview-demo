package com.lib.storyview.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.lib.storyview.R
import com.lib.storyview.data.StoriesItemModel

class StoryPagerAdapter(diff: DiffUtil.ItemCallback<StoriesItemModel> = StoryDiff()) :
    ListAdapter<StoriesItemModel, StoryHolder>(diff) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoryHolder {
        return StoryHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.view_story_page, parent, false)
        )
    }

    override fun onBindViewHolder(holder: StoryHolder, position: Int) {
        holder.bind(getItem(position), position)
    }
}

class StoryDiff : DiffUtil.ItemCallback<StoriesItemModel>() {
    override fun areItemsTheSame(oldItem: StoriesItemModel, newItem: StoriesItemModel): Boolean {
        return oldItem.page == newItem.page
    }

    override fun areContentsTheSame(oldItem: StoriesItemModel, newItem: StoriesItemModel): Boolean {
        val isSameSize = oldItem.items.size == newItem.items.size
        val hasSameContent = oldItem.items == newItem.items

        return isSameSize && hasSameContent
    }
}