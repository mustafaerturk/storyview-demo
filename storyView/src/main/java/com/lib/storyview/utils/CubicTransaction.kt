package com.lib.storyview.utils

import android.view.View
import androidx.viewpager2.widget.ViewPager2

class CubicTransaction : ViewPager2.PageTransformer {

    override fun transformPage(page: View, position: Float) {
        val deltaY = 0.4F

        page.pivotX = if (position < 0F) page.width.toFloat() else 0F
        page.pivotY = page.height * deltaY
        page.rotationY = 45F * position
    }

}