package com.lib.storyview.utils


sealed class StoryAction(open val page: Int) {

    data class PageAction(override val page: Int) : StoryAction(page)
    data class ToNextPage(override val page: Int) : StoryAction(page)
    data class ToPreviousPage(override val page: Int) : StoryAction(page)

    data class ToNextItemAction(override val page: Int) : StoryAction(page)

    data class StartProgress(override val page: Int, val duration: Long) : StoryAction(page)
    data class StopProgress(override val page: Int) : StoryAction(page)

}