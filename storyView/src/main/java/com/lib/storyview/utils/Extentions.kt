package com.lib.storyview.utils

import android.content.res.Resources.getSystem

val Int.tpDp: Int get() = (this / getSystem().displayMetrics.density).toInt()

val Int.toPx: Int get() = (this * getSystem().displayMetrics.density).toInt()