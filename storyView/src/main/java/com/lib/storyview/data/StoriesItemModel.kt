package com.lib.storyview.data

data class StoriesItemModel(
    val page: Int,
    val items: List<StoryItemModel>
)

data class StoryItemModel(
    val id: String,
    val type: StoryType,
    val url: String
)

enum class StoryType {
    IMAGE,
    VIDEO
}