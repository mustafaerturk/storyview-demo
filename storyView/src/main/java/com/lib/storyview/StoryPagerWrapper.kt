package com.lib.storyview

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.viewpager2.widget.ViewPager2
import com.lib.storyview.adapter.StoryPagerAdapter
import com.lib.storyview.data.StoriesItemModel
import com.lib.storyview.utils.CubicTransaction
import com.lib.storyview.utils.StoryAction.*


class StoryPagerWrapper(
    private val pager: ViewPager2,
    private val items: List<StoriesItemModel>
) : LifecycleObserver {

    private val pageChangeCallback = PageChangeCallback()

    private val storyPagerAdapter: StoryPagerAdapter = StoryPagerAdapter()
    private var currentPage = 0

    init {
        pager.setPageTransformer(CubicTransaction())
        pager.offscreenPageLimit = 1
        pager.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        pager.adapter = storyPagerAdapter

        with(StoryPageController) {
            add(observe()
                .filter {
                    it is ToNextPage || it is ToPreviousPage
                }.doOnNext {
                    pager.setCurrentItem(it.page, true)
                }
                .subscribe())
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        pager.registerOnPageChangeCallback(pageChangeCallback)
        storyPagerAdapter.submitList(items)
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause() {
        pager.unregisterOnPageChangeCallback(pageChangeCallback)
        StoryPageController.clear()
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        StoryPageController.dispose()
    }

    private inner class PageChangeCallback : ViewPager2.OnPageChangeCallback() {

        private var stopped = false

        override fun onPageScrollStateChanged(state: Int) {
            super.onPageScrollStateChanged(state)

            when (state) {
                ViewPager2.SCROLL_STATE_IDLE -> {
                    if (stopped) {
                        StoryPageController.submit(PageAction(pager.currentItem))
                        currentPage = pager.currentItem
                        stopped = false
                    }
                }
                ViewPager2.SCROLL_STATE_DRAGGING -> {
                    if (!stopped) {
                        StoryPageController.submit(StopProgress(pager.currentItem))
                        stopped = true
                    }
                }

                ViewPager2.SCROLL_STATE_SETTLING -> {
                    if (!stopped) {
                        StoryPageController.submit(StopProgress(pager.currentItem))
                        stopped = true
                    }
                }
            }
        }
    }
}


