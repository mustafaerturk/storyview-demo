package com.lib.storyview

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.GestureDetector
import android.view.Gravity
import android.view.MotionEvent
import android.view.MotionEvent.ACTION_UP
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.core.view.GestureDetectorCompat
import com.lib.storyview.data.StoriesItemModel
import com.lib.storyview.data.StoryItemModel
import com.lib.storyview.factroy.StoryFactory
import com.lib.storyview.factroy.StoryGenerator
import com.lib.storyview.factroy.StoryStoppable
import com.lib.storyview.progress.ProgressState
import com.lib.storyview.progress.StoryProgressView
import com.lib.storyview.utils.StoryAction.*
import com.lib.storyview.utils.toPx


class StoryView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private val mDetector: GestureDetectorCompat = GestureDetectorCompat(context, GestureListener())

    var storiesItem: StoriesItemModel? = null
        set(value) {
            field = value
            stories.addAll(field!!.items)
            stories.forEach { _ ->
                val progress = StoryProgressView(context, field!!.page)
                progressBarContainer.addView(progress, createProgressParams())
            }
        }

    private val storyFactory = StoryFactory(context)
    private val progressBarContainer: LinearLayout = LinearLayout(context)
    private val storyItemContainer: LinearLayout = LinearLayout(context)
    private val stories = arrayListOf<StoryItemModel>()

    private var dataContainer = StoryViewDataContainer(0, 0, null)
    private var storyGenerator: StoryGenerator? = null
    private var currentProgress: StoryProgressView? = null

    init {
        initStoryItemContainer()
        initProgressContainer()
        initController()
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        return if (mDetector.onTouchEvent(event)) {
            true
        } else {
            if (event?.action == ACTION_UP) {
                if (currentProgress?.status == ProgressState.STOPPED) {
                    currentProgress?.resume()
                    if (storyGenerator is StoryStoppable?) {
                        (storyGenerator as? StoryStoppable)?.resume()
                    }
                }
            }

            super.onTouchEvent(event)
        }
    }

    private fun initProgressContainer() {
        progressBarContainer.setBackgroundColor(
            ContextCompat.getColor(
                context,
                android.R.color.holo_orange_dark
            )
        )

        val params = LayoutParams(LayoutParams.MATCH_PARENT, 50.toPx)
        params.gravity = Gravity.TOP
        progressBarContainer.gravity = Gravity.CENTER
        addView(progressBarContainer, params)
    }

    private fun initController() {
        with(StoryPageController) {
            add(observe()
                .filter {
                    it.page == storiesItem?.page
                }
                .filter { action ->
                    when (action) {
                        is ToNextItemAction -> true
                        is StartProgress -> true
                        is PageAction -> true
                        is StopProgress -> true
                        else -> {
                            false
                        }
                    }
                }
                .doOnNext {
                    dataContainer = dataContainer.copy(page = it.page)
                    when (it) {
                        is ToNextItemAction -> toNextItem()
                        is PageAction -> start()
                        is StopProgress -> stop()
                        is StartProgress -> start(it.duration)
                    }
                }
                .doOnDispose {
                    stop()
                }
                .subscribe()
            )
        }
    }

    private fun initStoryItemContainer() {
        val params1 = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
        addView(storyItemContainer, params1)

        storyItemContainer.setBackgroundColor(
            ContextCompat.getColor(
                context,
                android.R.color.holo_blue_dark
            )
        )
    }

    private fun createProgressParams(): ViewGroup.LayoutParams {
        val params = LinearLayout.LayoutParams(0.toPx, 10.toPx)
        params.weight = 1f
        params.marginEnd = 5.toPx
        params.marginStart = 5.toPx
        return params
    }

    private fun toNextItem() {
        val currentItemIndex = dataContainer.currentItemIndex + 1

        if (currentItemIndex < stories.size) {
            createStory(currentItemIndex)
        } else {
            stop()
            StoryPageController.submit(ToNextPage(storiesItem!!.page + 1))
        }
    }

    private fun toPrevItem() {
        val currentItemIndex = dataContainer.currentItemIndex - 1

        if (currentItemIndex >= 0) {
            createStory(currentItemIndex)
        } else {
            if (storiesItem!!.page > 0) {
                stop()
                StoryPageController.submit(ToPreviousPage(storiesItem!!.page - 1))
            }
        }
    }

    private fun createStory(index: Int) {
        dataContainer = dataContainer.copy(
            currentItemIndex = index,
            currentStoryItem = stories[index]
        )

        if (storyGenerator is StoryStoppable?) {
            if (storyGenerator is StoryStoppable?) {
                (storyGenerator as? StoryStoppable)?.release()
            }
        }
        storyItemContainer.removeAllViews()

        storyGenerator = storyFactory.create(
            type = dataContainer.currentStoryItem!!.type
        )


        storyItemContainer.addView(
            storyGenerator?.create(dataContainer.currentStoryItem!!.url, dataContainer.page),
            LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
        )
    }

    private fun stop() {
        currentProgress?.stop()
        if (storyGenerator is StoryStoppable?) {
            (storyGenerator as? StoryStoppable)?.stop()
        }
    }

    private fun start(duration: Long) {
        currentProgress = progressBarContainer.getChildAt(dataContainer.currentItemIndex) as StoryProgressView
        currentProgress!!.reset()
        currentProgress!!.start(duration)
    }

    fun start() {
        if (storyGenerator == null) {
            createStory(dataContainer.currentItemIndex)
        } else {
            currentProgress?.restart()

            if (storyGenerator is StoryStoppable?) {
                (storyGenerator as? StoryStoppable)?.restart()
            }
        }
    }

    private inner class GestureListener : GestureDetector.SimpleOnGestureListener() {

        override fun onDown(e: MotionEvent?): Boolean {
            return true
        }

        override fun onSingleTapUp(event: MotionEvent?): Boolean {
            event?.let {
                if (it.x < width / 2) {
                    if (dataContainer.page == 0 && dataContainer.currentItemIndex == 0) {
                        return false
                    }
                    currentProgress?.reset()
                    toPrevItem()
                } else {
                    currentProgress?.completed()
                    toNextItem()
                }
            }

            return true
        }

        override fun onLongPress(p0: MotionEvent?) {
            currentProgress?.stop()
            if (storyGenerator is StoryStoppable?) {
                (storyGenerator as? StoryStoppable)?.stop()
            }
        }
    }
}

data class StoryViewDataContainer(
    val page: Int,
    val currentItemIndex: Int,
    val currentStoryItem: StoryItemModel?
)